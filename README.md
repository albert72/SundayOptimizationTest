# SundayOptimizationTest

## Task

There are a few issues with this project. Please fix them and build a working APK for the client. If you do not have an Android device, a build working on iOS or even on an Android emulator is enough. The game is a placeholder and the end result should stay the same, please do not try to make new levels, better models etc.. Other than this, you are free to change everything.

Please fix the error and check the code, physics, materials and build settings to see if there are any improvements you can make. There is no specific output we require, please do whatever you can to improve and don't overthink it. It would be ideal if you could create a private repository on GitHub or GitLab and invite the corresponding person to review it.

I have written the complaints of the imaginary client below.

## My Game Doesn't Work Properly

 - MyEventSystem class can't find GameAnalytics required to send my level start and finish events. I imported the unitypackage and found a script that handles the events but it's not working.
 - My game has performance issues on mobile devices despite it only having a few objects and scripts.
 - My controls work differently depending on the frames per second I run it at.
 - There is something wrong with my git repository, a lot of irrelevant files get added to my pushes.

 ## Bonus Issues
 
 - I have a hard time managing my levels and it's extremely hard for me to add new levels.
 - There's something wrong with my lighting. It looks different when I build it and when I edit it in the editor.
